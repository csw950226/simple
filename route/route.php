<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});

Route::get('hello/:name', 'index/hello');

Route::any('upload', 'api/util.file/upload')->allowCrossDomain();;

$v = request()->header('version');
if($v==null) $v = "v1";

return [
// api版本路由
    //'api/:version/:controller'=>'api/:version.:controller/index',// 省略方法名时 网址格式:http://www.xxx.com/api/v1/index
    // 有方法名时 网址格式:http://www.xxx.com/api/v1/index/login
//    'api/:version/:controller/:function'=>'api/:version.:controller/:function',
//    'api/:version/:module/:controller/:function'=>'api/:module.:version.:controller/:function',
    'api/:version/:module/:controller/:function'=>[
        'api/:version.:module.:controller/:function', ['middleware' => [\app\http\middleware\Check::class]]
    ],

//    //api版本控制
//    'api/:controller$'=>['api/'.$v.'.:controller/index',['method' => 'get']],
//    'api/:controller/:function$'=>'api/'.$v.'.:controller/:function',
//
//    //资源路由
//    '__rest__'=>[
//        //api
//        'api/house'=>['api/'.$v.'.house',['only'=>['index','read','update','delete']]],
//        'api/book'=>['api/'.$v.'.book',['only'=>['index','read','save','delete']]],
//        'api/book_rent'=>['api/'.$v.'.book_rent',['only'=>['index','read','save']]],
//    ]
];
