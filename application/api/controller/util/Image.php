<?php

namespace app\api\controller\util;

use think\Controller;
use think\Response;

class Image extends Controller {

    /**
     * 获取图片缩略图(指定宽高从中间裁切，默认150*150)
     * 前端使用地址: http://www.xxx.com/ImageCrop?url=(图片地址)&w=(宽)&h=(高)
     * @return Response
     */
    public function crop() {
        $img_url = input('param.url');
        $width = input('param.w');
        $height = input('param.h');
        if ($img_url)
            return \service\utils\Image::thumb($img_url,\service\utils\Image::TYPE_CROP,array('width'=>$width,'height'=>$height));
        else
            return http_response('非法请求',403);
    }

    /**
     * 获取图片缩略图(按等比例缩放，取值1-100，默认50)
     * 前端使用地址: http://www.xxx.com/ImageScale?url=(图片地址)&p=(比例)
     * @return Response
     */
    public function scale() {
        $img_url = input('param.url');
        $scale = input('param.p');
        if ($img_url)
            return \service\utils\Image::thumb($img_url,\service\utils\Image::TYPE_SCALE,array('scale'=>$scale));
        else
            return http_response('非法请求',403);
    }
}