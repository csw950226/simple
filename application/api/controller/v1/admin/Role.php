<?php


namespace app\api\controller\v1\admin;


use app\api\ApiController;
use app\model\ModelMenu;
use app\model\ModelRole;

class Role extends ApiController
{
    /**
     * 权限组列表
     */
    public function roleList()
    {
        $page = input('page', 1);
        $limit = input('limit', 20);
        $model = new ModelRole();
        $data = $model->field('id,name,auth_ids,flag,create_time,update_time')->page($page, $limit)->select();
        $count = $model->count('*');
        if ($data) {
            foreach ($data as &$value) {
                $value['status'] = $value['flag'] == 1 ? '启用' : '禁用';
            }
            $result = [
                'count' => $count,
                'list' => $data,
            ];
            return api_success($result);
        } else {
            return api_list_not_more();
        }
    }

    /**
     * 权限组创建
     */
    public function roleCreate()
    {
        $info = input('');
        if (empty($info['name'])) {
            return api_param_error();
        }
        $model = new ModelRole();
        $res = $model->save($info);
        if ($res) {
            return api_success('添加成功');
        } else {
            return api_error('添加失败');
        }
    }

    /**
     * 权限组修改
     */
    public function roleEdit()
    {
        $info = input('');
        if (empty($info['id'])) {
            return api_param_error();
        }
        $model = new ModelRole();
        $res = $model->save($info, ['id' => $info['id']]);
        if ($res) {
            return api_success('修改成功');
        } else {
            return api_error('修改失败');
        }
    }

    /**
     * 权限组详情
     */
    public function roleDetail()
    {
        $id = input('id');
        if (empty($id)) {
            return api_param_error();
        }
        $model = new ModelRole();
        $data = $model->field('id,name,flag')->find($id);
        if ($data) {
            $data['flag'] = (string)$data['flag'];
            return api_success($data);
        } else {
            return api_list_not_more();
        }
    }

    /**
     * 权限组删除
     */
    public function roleDelete()
    {
        $id = input('id');
        if (empty($id)) {
            return api_param_error();
        }
        $model = new ModelRole();
        $res = $model::destroy($id);
        if ($res) {
            return api_success('删除成功');
        } else {
            return api_error('删除失败');
        }
    }

    /**
     * 权限组全部权限以及拥有的权限
     */
    public function getMenu()
    {
        $id = input('id');
        if (empty($id)) {
            return api_param_error();
        }
        $menuModel = new ModelMenu();
        $menu = $menuModel->getMenu([], 'id,name,pid,url,level,status,is_menu');
        $roleModel = new ModelRole();
        $admin = $roleModel->field('id,auth_ids')->find($id);
        $authIds = !empty($admin['auth_ids']) ? explode(',', $admin['auth_ids']) : [];
        $menu = recursive_make_tree($menu, 'id', 'pid', 'children', 0);
        $result = [
            'menus' => $menu, // 全部权限
            'auth_ids' => $authIds, // 拥有的权限
        ];
        return api_success($result);
    }

    /**
     * 获取全部可用权限组
     */
    public function getRoles()
    {
        $model = new ModelRole();
        $data = $model->where(['flag' => 1])->field('id,name,auth_ids,flag,create_time,update_time')->select();
        if ($data) {
            return api_success($data);
        } else {
            return api_list_not_more();
        }
    }

}