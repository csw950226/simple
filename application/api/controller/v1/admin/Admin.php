<?php


namespace app\api\controller\v1\admin;


use app\api\ApiController;
use app\facade\Jwt;
use app\model\ModelAdmin;
use app\model\ModelMenu;
use think\facade\Request;

class Admin extends ApiController
{
    public function login()
    {
        $username = input('username');
        $password = input('password');
        $model = new ModelAdmin();
        $user = $model->where(['username' => $username])
            ->field('id,fullname,username,email,pwd')->findOrEmpty();
        if (!empty($user)) {
            if (password_verify($password, $user['pwd'])) {
                $token = Jwt::getToken($user['id']);
                $data = [
                    'user_id' => $user['id'],
                    'name' => $user['fullname'],
                    'token' => $token,
                ];
                return api_success($data);
            } else {
                return api_error('用户名或密码错误');
            }
        } else {
            return api_error('用户名或密码错误');
        }
    }

    /**
     * 获取管理员信息
     */
    public function getUserInfo()
    {
        $token = Request::header('token');
        $jwtInfo = Jwt::verifyToken($token);
        if ($jwtInfo['code'] != 0 ){
            return api_error($jwtInfo['msg']);
        }
        $userId = $jwtInfo['data']['user_id'];
        $adminModel = new ModelAdmin();
        $user = $adminModel->field('id,fullname,username,email,pwd')->findOrEmpty($userId);
        $model = new ModelMenu();
        $where = [
            'status' => 1,
            'type' => 1,
        ];
        if ($user['id'] == 1) {
            $data = $model->where($where)->field('id,name,url,level,pid,type')->select();
        } else {
            $data = $model->where($where)->select();
        }
        foreach ($data as &$value) {
            $value['path'] = $value['url'];
            $value['meta'] = [
                'title' => $value['name'],
                'icon' => '',
            ];
        }
        $data = recursive_make_tree($data, 'id', 'pid', 'children', 0);
        $result = [
            'name' => $user['fullname'],
            'avatar' => 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
            'roles' => $data,
        ];
        return api_success($result);
    }

    public function logout()
    {
        return api_success('退出登录');
    }

    /**
     * 管理员列表
     */
    public function adminUser()
    {
        $page = input('page', 1);
        $limit = input('limit', 20);
        $model = new ModelAdmin();
        $data = $model->page($page, $limit)->select();
        $count = $model->count('*');
        if ($data) {
            $result = [
                'count' => $count,
                'list' => $data,
            ];
            return api_success($result);
        } else {
            return api_list_not_more();
        }
    }

    /**
     * 管理员创建
     */
    public function createUser()
    {
        $info = input('');
        if (empty($info['username']) || empty($info['pwd']) || empty($info['repwd'])) {
            return api_param_error();
        }
        if ($info['pwd'] != $info['repwd']) {
            return api_error('两次密码不相等');
        }
        $model = new ModelAdmin();
        $have = $model->where(['username' => $info['username']])->field('id')->find();
        if (!empty($have)) {
            return api_error('账户已存在');
        }
        $info['pwd'] = password_hash($info['pwd'], 1);
        $res = $model->save($info);
        if ($res) {
            return api_success('添加成功');
        } else {
            return api_error('添加失败');
        }
    }

    /**
     * 管理员修改
     */
    public function editUser()
    {
        $info = input('');
        if (empty($info['id'])) {
            return api_param_error();
        }
        $model = new ModelAdmin();
        $res = $model->save($info, ['id' => $info['id']]);
        if ($res) {
            return api_success('修改成功');
        } else {
            return api_error('修改失败');
        }
    }

    /**
     * 管理员详情
     */
    public function userDetail()
    {
        $id = input('id');
        if (empty($id)) {
            return api_param_error();
        }
        $model = new ModelAdmin();
        $data = $model->find($id);
        if ($data) {
            return api_success($data);
        } else {
            return api_list_not_more();
        }
    }

    /**
     * 管理员删除
     */
    public function userDelete()
    {
        $id = input('id');
        if (empty($id)) {
            return api_param_error();
        }
        $model = new ModelAdmin();
        $res = $model::destroy($id);
        if ($res) {
            return api_success('删除成功');
        } else {
            return api_error('删除失败');
        }
    }
}