<?php


namespace app\facade;


use app\api\controller\Index;
use think\Facade;

/**
 * @method getAuthList() static
 */
class Admin extends Facade
{
    protected static function getFacadeClass()
    {
        return Index::class;
        return 'app\api\controller\Index';
    }
}