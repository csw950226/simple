<?php


namespace app\facade;


use think\Facade;

/**
 * @method getToken($user_id) static
 * @method verifyToken($token) static
 */
class Jwt extends Facade
{
    protected static function getFacadeClass()
    {
        return \service\auth\Jwt::class;
    }
}