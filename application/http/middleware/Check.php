<?php


namespace app\http\middleware;


use app\facade\Jwt;
use think\facade\Request;
use think\facade\Response;

class Check
{
    public function handle($request, \Closure $next)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credentials: true');//头部是否包含cookie
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,Authorization,token");
        if (Request::instance()->isOptions())
            exit();
        $whiteList = [
            'login',
            'logout',
            'test1',
        ];
//        dump($request->function);
        //验证后台用户信息
        if ($request->module == 'admin' && !in_array($request->function, $whiteList)) {
            $token = $request->header('token');
            if (empty($token)) {
                return api_token_error();
            } else {
                $res = Jwt::verifyToken($token);
                if ($res['code'] != 0) {
                    return api_token_error();
                }
            }
        }
        return $next($request);
    }
}