<?php


namespace app\model;


use think\Model;

class ModelMenu extends Model
{
    protected $table = 'tb_menu';
    protected $field = true;

    /**
     * @param array $where 条件
     * @param string $field 字段
     * @param bool $all 是否全部展示
     * @param int $page 页码
     * @param int $limit 数量
     * @param string $order 排序
     */
    public function getMenu($where = [], $field = '*', $all = true, $page = 1, $limit = 20, $order = 'id asc')
    {
        $model = new ModelMenu();
        $data = $all ? $model->where($where)->field($field)->order($order)->select() :
            $model->where($where)->field($field)->order($order)->page($page, $limit)->select();
//        if (!empty($data)) {
//            foreach ($data as &$value) {
//                $value['type'] = (string)$value['type'];
//            }
//        }
        return $data;
    }
}