<?php


namespace app\model;


use think\Model;

class ModelAdmin extends Model
{
    protected $table = 'tb_admin';
    protected $field = true;
}