<?php


namespace app\model;


use think\Model;

class ModelRole extends Model
{
    protected $table = 'tb_role';
    protected $field = true;
}