<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**
 * @param array $list 要转换的数据集
 * @param string $pk 自增字段（栏目id）
 * @param string $pid parent标记字段
 * @param string $child 孩子节点key
 * @param integer $root 根节点标识
 * @return array
 */
function recursive_make_tree($list, $pk = 'Fid', $pid = 'pid', $child = '_child', $root = 0)
{
    $tree = [];
    foreach ($list as $key => $value) {
        if ($value[$pid] == $root) {
            unset($list[$key]);
            if (!empty($list)) {
                $childVal = recursive_make_tree($list, $pk, $pid, $child, $value[$pk]);
                if (!empty($childVal)) {
                    $value[$child] = $childVal;
                }
            }
            $tree[] = $value;
        }
    }
    return $tree;
}

/************ API响应 *************/
/**
 * @param string $data
 * @param int $code
 * @param string $type
 * @param array $header
 * @param array $options
 * @return think\response
 */
function http_response($data = '非法请求', $code = 403, $type = 'html', array $header = [], $options = []) {
    return \service\http\ApiOut::httpResponse($data,$type,$code,$header,$options);
}

function api_success($data = '', array $header = [], $options = []) {
    return \service\http\ApiOut::success($data,$header,$options);
}

function api_error($msg = '', array $header = [], $options = []) {
    return \service\http\ApiOut::error($msg,$header,$options);
}

function api_param_error($msg = '', array $header = [], $options = []) {
    return \service\http\ApiOut::paramError($msg,$header,$options);
}

function api_sign_error(array $header = [], $options = []) {
    return \service\http\ApiOut::signError($header,$options);
}

function api_token_error(array $header = [], $options = []) {
    return \service\http\ApiOut::tokenError($header,$options);
}

function api_repeat_error(array $header = [], $options = []) {
    return \service\http\ApiOut::repeatError($header,$options);
}

function api_server_error(array $header = [], $options = []) {
    return \service\http\ApiOut::serverError($header,$options);
}

function api_unknown_error(array $header = [], $options = []) {
    return \service\http\ApiOut::unknownError($header,$options);
}

function api_not_permission(array $header = [], $options = []) {
    return \service\http\ApiOut::notPermission($header,$options);
}

function api_list_not_more(array $header = [], $options = []) {
    return \service\http\ApiOut::listNotMore($header,$options);
}

function api_login_overdue(array $header = [], $options = []) {
    return \service\http\ApiOut::loginOverdue($header,$options);
}

function api_login_elsewhere(array $header = [], $options = []) {
    return \service\http\ApiOut::loginElsewhere($header,$options);
}

if(!function_exists('downLoadPic')) {
    function downLoadPic($url = 'http://ossiden.qqzaiqdx.cn/Daka/20180312/2f2b0e8086f4fa76c76d3508cbc7df04.png', $path = PUBLIC_PATH){
        $filename = pathinfo($url, PATHINFO_BASENAME);
        if(!file_exists($path.$filename)){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            $file = curl_exec($ch);
            curl_close($ch);
            $filename = pathinfo($url, PATHINFO_BASENAME);
            $resource = fopen($path.'/' . $filename, 'a');
            fwrite($resource, $file);
            fclose($resource);
        }
        return $filename;
    }
}