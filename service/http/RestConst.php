<?php

namespace service\http;

/**
 * Class RestConst 规范接口返回内容
 * REST Api 当返回code=0时，数据在data字段，当返回非0时，提示内容在msg字段
 */
class RestConst {

    const CODE = [
        'success'             => 0,   //操作成功返回
        'error'               => 1,   //操作失败、异常、错误返回
        'params_error'        => 2,   //参数错误返回
        'sign_error'          => 3,   //签名错误返回
        'token_error'         => 4,   //令牌错误返回
        'repeat_error'        => 5,   //操作频繁返回
        'server_error'        => 6,   //服务器代码逻辑错误返回
        'unknown_error'       => 7,   //未知错误返回
        'not_permission'      => 11,  //没有权限访问返回
        'list_not_more'       => 101, //分页接口没有更多内容返回
        'login_overdue'       => 901, //账户登录过期返回
        'login_elsewhere'     => 902, //账号在别处登录返回
    ];

    const MSG = [
        'success'             => '',
        'error'               => '操作失败！请重试！',
        'params_error'        => '参数错误',
        'sign_error'          => '签名错误',
        'token_error'         => 'Token错误',
        'repeat_error'        => '操作频繁',
        'server_error'        => '服务器内部错误',
        'unknown_error'       => '未知错误',
        'not_permission'      => '没有权限访问',
        'list_not_more'       => '没有更多内容',
        'login_overdue'       => '登录过期，请重新登录！',
        'login_elsewhere'     => '账号在别处登录',
    ];

    /**
     * 定义输出格式
     * @param mixed $code
     * @param string $data
     * @param mixed $msg
     * @return array
     */
    public static function out($code = self::CODE['server_error'], $data = '', $msg = self::MSG['server_error']) {
        if ($code === 0) {
            return [
                'code' => 0,
                'data' => $data
            ];
        } else {
            return [
                'code' => $code,
                'msg' => $msg
            ];
        }
    }
}