<?php

namespace service\http;

class HttpRequest {

    const REQUEST_FROM_WINDOWS = 'windows';     //windows电脑浏览器
    const REQUEST_FROM_MAC = 'mac';             //mac电脑浏览器
    const REQUEST_FROM_IPAD = 'ipad';           //iPad浏览器
    const REQUEST_FROM_IPHONE = 'iphone';       //iPhone浏览器
    const REQUEST_FROM_ANDROID = 'android';     //android浏览器

    /**
     * 请求来自什么终端
     * @param $user_agent $_SERVER['HTTP_USER_AGENT']
     * @return string
     */
    public static function fromTerminal($user_agent) {
        $agent = strtolower($user_agent);
        $is_windows = (strpos($agent, 'windows nt')) ? true : false;
        $is_mac = (strpos($agent, 'mac os')) ? true : false;
        $is_ipad = (strpos($agent, 'ipad')) ? true : false;
        $is_iphone = (strpos($agent, 'iphone')) ? true : false;
        $is_android = (strpos($agent, 'android')) ? true : false;
        if ($is_windows)
            return self::REQUEST_FROM_WINDOWS;
        elseif ($is_android)
            return self::REQUEST_FROM_ANDROID;
        elseif ($is_ipad)
            return self::REQUEST_FROM_IPAD;
        elseif ($is_iphone)
            return self::REQUEST_FROM_IPHONE;
        elseif ($is_mac)
            return self::REQUEST_FROM_MAC;
        else
            return self::REQUEST_FROM_WINDOWS;
    }
}