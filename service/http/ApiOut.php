<?php

namespace service\http;

class ApiOut {

    /**
     * HTTP状态码 非200的响应(默认403非法请求)
     * @param string $data
     * @param int $code
     * @param string $type
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function httpResponse($data = '非法请求', $code = 403, $type = 'html', array $header = [], $options = []) {
        return HttpResponse::createHttp($data,$type,$code,$header,$options);
    }

    /********* HTTP状态码 200的响应 **********/
    /**
     * api请求的正常或成功返回
     * @param $data string|object|array
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function success($data = '', array $header = [], $options = []) {
        $code = RestConst::CODE['success'];
        return HttpResponse::createRest($code,$data,'',$header,$options);
    }

    /**
     * api请求的非正常或非成功返回
     * @param string $type
     * @param string $msg
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    private static function noSuccess($type = 'server_error', $msg = '', array $header = [], $options = []) {
        $code = RestConst::CODE[$type];
        $msg = $msg ? $msg : RestConst::MSG[$type];
        return HttpResponse::createRest($code,'',$msg,$header,$options);
    }

    /**
     * api请求的异常或失败返回
     * @param string $msg
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function error($msg = '', array $header = [], $options = []) {
        return self::noSuccess('error',$msg,$header,$options);
    }

    /**
     * api请求的参数错误返回
     * @param string $msg
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function paramError($msg = '', array $header = [], $options = []) {
        return self::noSuccess('params_error',$msg,$header,$options);
    }

    /**
     * api请求的Token错误返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function tokenError(array $header = [], $options = []) {
        return self::noSuccess('token_error',$header,$options);
    }

    /**
     * api请求的签名错误返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function signError(array $header = [], $options = []) {
        return self::noSuccess('sign_error',$header,$options);
    }

    /**
     * api请求的频繁请求错误返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function repeatError(array $header = [], $options = []) {
        return self::noSuccess('repeat_error',$header,$options);
    }

    /**
     * api请求的服务器内部错误错误返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function serverError(array $header = [], $options = []) {
        return self::noSuccess('server_error',$header,$options);
    }

    /**
     * api请求的未知错误错误返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function unknownError(array $header = [], $options = []) {
        return self::noSuccess('unknown_error',$header,$options);
    }

    /**
     * api请求的没有权限访问返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function notPermission(array $header = [], $options = []) {
        return self::noSuccess('not_permission',$header,$options);
    }

    /**
     * api请求的分页加载没有更多内容返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function listNotMore(array $header = [], $options = []) {
        return self::noSuccess('list_not_more',$header,$options);
    }

    /**
     * api请求的账号登录过期返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function loginOverdue(array $header = [], $options = []) {
        return self::noSuccess('login_overdue',$header,$options);
    }

    /**
     * api请求的账号在别处登录返回
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function loginElsewhere(array $header = [], $options = []) {
        return self::noSuccess('login_elsewhere',$header,$options);
    }

}