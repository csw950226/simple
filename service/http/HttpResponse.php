<?php

namespace service\http;

use think\Response;

/**
 * 响应对象
 * Class HttpResponse 继承Response，增加我们自己的REST接口规范输出部分
 */
class HttpResponse extends Response {

    protected $r_code; //int
    protected $r_data; //string|object|array
    protected $r_msg;  //string

    /**
     * HttpResponse constructor. 构造函数
     * @param array|string $data
     * @param int $code
     * @param array $header
     * @param array $options
     */
    public function __construct($data = [], $code = 200, array $header = [], array $options = []) {
        if (is_array($data) && isset($data['code']) && isset($data['data']) && isset($data['msg'])) {
            $this->r_code = $data['code'];
            $this->r_data = $data['data'];
            $this->r_msg = $data['msg'];
        }
        parent::__construct($data, $code, $header, $options);
    }

    /**
     * 构建HTTP状态码为200的自己REST风格JSON输出
     * @param $r_code
     * @param $r_data
     * @param $r_msg
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function createRest($r_code, $r_data, $r_msg, array $header = [], $options = []) {
        return parent::create(RestConst::out($r_code, $r_data, $r_msg), 'json', 200, $header, $options);
    }

    /**
     * 构建HTTP状态码为非200的响应输出，默认403非法请求
     * @param $data
     * @param int $code
     * @param string $type
     * @param array $header
     * @param array $options
     * @return \think\Response
     */
    public static function createHttp($data = '非法请求', $code = 403, $type = 'html', array $header = [], $options = []) {
        return parent::create($data, $type , $code, $header, $options);
    }

}