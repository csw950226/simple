<?php


namespace service\auth;

use Firebase\JWT\JWT as J;

class Jwt
{
    /**
     * 获取token
     * @param $user_id
     * @return array
     */
    public function getToken($user_id)
    {
        $key = 'iden';  //这里是自定义的一个随机字串，应该写在config文件中的，解密时也会用，相当    于加密中常用的 盐  salt
        $time = time();
        $jwtData = [
            "iss" => "http://simple.iden.com",  //签发者 可以为空
            "aud" => "http://simple.iden.com", //面象的用户，可以为空
            "iat" => $time, //签发时间
            "nbf" => $time, //在什么时候jwt开始生效  （这里表示生成10秒后才生效）
            "exp" => $time + 86400 * 7, //token 过期时间
            "user_id" => $user_id, //token 过期时间
//            "data" => [
//                'user_id' => $user_id,
//            ], //记录的userid的信息，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
        ];

        $access_token = $jwtData;
        $access_token['scopes'] = 'role_access'; //token标识，请求接口的token
        $access_token['exp'] = $time + 86400; //access_token过期时间,这里设置2个小时

        $refresh_token = $jwtData;
        $refresh_token['scopes'] = 'role_refresh'; //token标识，刷新access_token
        $refresh_token['exp'] = $time + (86400 * 7); //refresh_token过期时间,这里设置30天

        $jsonList = [
            'access_token' => J::encode($access_token, $key),
            'refresh_token' => J::encode($refresh_token, $key),
//            'token_type'=>'bearer' //token_type：表示令牌类型，该值大小写不敏感，这里用bearer
        ];
        return $jsonList;
    }

    /**
     * 验证token是否合法
     * @param $token
     * @return int[]
     */
    public function verifyToken($token)
    {
        $key = 'iden'; //key要和签发的时候一样
        //Firebase定义了多个 throw new，我们可以捕获多个catch来定义问题，catch加入自己的业务，比如token过期可以用当前Token刷新一个新Token
        $result = [
            'code' => 0,
        ];
        try {
            J::$leeway = 60;//当前时间减去60，把时间留点余地
            $decoded = J::decode($token, $key, ['HS256']); //HS256方式，这里要和签发的时候对应
            $arr = (array)$decoded;
            $result['msg'] = '正常';
            $result['data'] = $arr;
            return $result;
        } catch (\Firebase\JWT\SignatureInvalidException $e) {  //签名不正确
            $result['code'] = 1;
            $result['msg'] = '签名不正确';
            return $result;
        } catch (\Firebase\JWT\BeforeValidException $e) {  // 签名在某个时间点之后才能用
            $result['code'] = 2;
            $result['msg'] = '签名尚未启用';
            return $result;
        } catch (\Firebase\JWT\ExpiredException $e) {  // token过期
            $result['code'] = 3;
            $result['msg'] = '签名过期';
            return $result;
        } catch (\Exception $e) {  //其他错误
            $result['code'] = 4;
            $result['msg'] = '其他错误';
            return $result;
        }

    }
}