<?php


namespace service\auth;


use app\model\ModelMenu;

class Menu
{
    public function getMenu($where = [], $field = '*', $All = true, $page = 1, $limit = 20, $order = 'id asc')
    {
        $model = new ModelMenu();
        $data = $All ? $model->where($where)->field($field)->order($order)->select() :
            $model->where($where)->field($field)->order($order)->page($page, $limit)->select();
        return $data;
    }
}