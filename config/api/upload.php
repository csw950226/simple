<?php
define('FILE_UPLOAD_PATH', PUBLIC_PATH . 'uploads/');
return [
    'file_upload_path_base' => FILE_UPLOAD_PATH,
    'file_upload_path' => [
        'admin' => FILE_UPLOAD_PATH . 'admin/',
        'index' => FILE_UPLOAD_PATH . 'index/',
    ]
];